PanDelay {

	*ar { |in, pos = 0|
		var left, right;
		left = pos.linlin(-90, 0, 0.00058, 0);
		right = pos.linlin(0, 90, 0, 0.00058);
		^DelayC.ar(in, 0.1, [left, right]);
	}

}

Doppler {

	*ar { |in, distance, maxDistance = 500|
		^DelayC.ar(in, maxDistance/344, distance/344);
	}

}

Distance {

	*ar { |in, distance, minDistance = 1, maxDistance = 100, minFreq = 600, maxFreq = 20000, absptCoeff = 100000|
		var freq;
		distance = distance.clip(minDistance, maxDistance);
		freq = 3.0/distance * absptCoeff;
		in = LPF.ar(in, freq.clip(minFreq, maxFreq));
		in = in * distance.reciprocal;
		^in
	}

}

DistanceFFT {

	*ar { |in, distance, thresh = 0.5, smoothing = 0.9, minDistance = 1, maxDistance = 100, minFreq = 600, maxFreq = 20000|
		var freq;
		distance = distance.clip(minDistance, maxDistance);
		freq = 3.0/distance * 100000;
		in = LPF.ar(in, freq.clip(minFreq, maxFreq));
		in = in * distance.reciprocal;
		in = FFT(LocalBuf(2048*16), in, 0.125, 1);
		in = PV_LocalMax(in, thresh);
		in = PV_MagSmooth(in, smoothing);
		in = IFFT(in)
		^in
	}

}

