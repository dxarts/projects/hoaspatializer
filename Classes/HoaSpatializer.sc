HoaSpatializer {

	*ar{ arg in, masterX, masterY, masterZ, width, maxDistance = 200, minDistance = 0.5, order = AtkHoa.defaultOrder;
		var cart, amplitude, radialDistance;
		var phi, theta, chain;

		cart = Cartesian(Lag.kr(masterX), Lag.kr(masterY), Lag.kr(masterZ));

		theta = cart.theta;
		phi = cart.phi;
		radialDistance = cart.rho;

		// apply distance modeling for high freq absorption and amplitude scaling
		chain = Distance.ar(in, radialDistance, minDistance, maxDistance);

		// use a delay line based on the distance to simulate a doppler shift
		chain = Doppler.ar(chain, radialDistance, maxDistance);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaZoom.ar(chain, (radialDistance/width).atan, 0, 0, AtkHoa.refRadius, order);

		// pan the sound using ambisonics
		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		// prepare signal for near-field control
		chain = HPF.ar(HPF.ar(chain, 20), 20);

		chain = HoaNFCtrl.ar(chain, radialDistance.clip(minDistance, maxDistance), AtkHoa.refRadius, order);

		^chain

	}
}


HoaSpatializerNoAmp {

	*ar{ arg in, masterX, masterY, masterZ, width, maxDistance = 100, minDistance = 0.5, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;

		// lets set a few variables first
		hiFreqAttenCoeff = 30000;
		minAttenDist = 5;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag2.kr(masterX, 10);
		masterY = Lag2.kr(masterY, 10);
		masterZ = Lag2.kr(masterZ, 10);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.clip(minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(minDistance, maxDistance).reciprocal;

		// lowpass the src and apply envelope and amplitude
		// chain = LPF.ar(in, freq);

		chain = HPF.ar(HPF.ar(in, 20), 20);

		chain = HoaNFCtrl.ar(chain, radialDistance.clip(1, 10), AtkHoa.refRadius, order);

		// use a delay line based on the distance to simulate a doppler shift
		// chain = DelayC.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaZoom.ar(chain, radialDistance.atan2(width), 0, 0, 1.5, order);

		chain = HoaRotate.ar(HoaTumble.ar(chain, Lag2.kr(phi, 5), order), Lag2.kr(theta, 5), order);

		^chain

	}
}
HoaSpatializerNoDelay {

	*ar{ arg in, masterX, masterY, masterZ, width, maxDistance = 100, minDistance = 0.5, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;

		// lets set a few variables first
		hiFreqAttenCoeff = 30000;
		minAttenDist = 5;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.linexp(minAttenDist, maxDistance, minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(minDistance, maxDistance).reciprocal;

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(in, freq);

		chain = HoaNFCtrl.ar(chain, radialDistance.clip(minDistance, 10), AtkHoa.refRadius, order);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaZoom.ar(HPF.ar(chain, 20), radialDistance.atan2(width), 0, 0, radialDistance.clip(minDistance, 10), 'energy', 0, order);

		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		^(chain * amplitude)

	}
}
HoaEncodeSpatializer {

	*ar{ arg in, masterX, masterY, masterZ, width, maxDistance = 100, minDistance = 0, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;

		// lets set a few variables first
		hiFreqAttenCoeff = 30000;
		minAttenDist = 5;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.clip(minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(1 + minDistance, maxDistance).reciprocal;

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(in, freq);

		// use a delay line based on the distance to simulate a doppler shift
		chain = DelayC.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width

		chain = HoaEncodeDirection.ar(HPF.ar(chain, 20), theta, phi, radialDistance.clip(0.5, 10), order);

		^(chain  * amplitude)

	}
}

HoaSpatializerReverb {

	*ar{ arg in, masterX, masterY, masterZ, width, revTimeHigh = 1.0, revTimeLow = 1.0, maxDistance = 200, minDistance = 0.5, mix = 0.25, crossover = 3000, order = AtkHoa.defaultOrder;
		var cart, amplitude, radialDistance;
		var phi, theta, chain, reverb, predelay, speedOfSound = 344;

		cart = Cartesian(masterX, masterY, masterZ);

		theta = cart.theta;
		phi = cart.phi;
		radialDistance = cart.rho;

		// calculate the predely of the first reflection based on the assumption it bounces off the floor, average human height 1.7m
		// minus the delay introduced by Doppler
		predelay = (((radialDistance/2).squared + 1.7.squared).sqrt * 2)/speedOfSound - (radialDistance/speedOfSound);

		// apply distance modeling for high freq absorption and amplitude scaling
		chain = Distance.ar(in, radialDistance, minDistance, maxDistance);

		// use a delay line based on the distance to simulate a doppler shift
		chain = Doppler.ar(chain, radialDistance, maxDistance);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaZoom.ar(chain, (radialDistance/width).atan, 0, 0, AtkHoa.refRadius, order);

		// pan the sound using ambisonics
		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		// prepare signal for near-field control
		chain = HPF.ar(HPF.ar(chain, 20), 20);

		chain = HoaNFCtrl.ar(chain, radialDistance.clip(minDistance, maxDistance), AtkHoa.refRadius, order);

		reverb = HoaReverb.ar(chain * mix * radialDistance.clip(minDistance, maxDistance).reciprocal.sqrt, 1.0, predelay, crossover, order: order, t60high: revTimeHigh, t60low: revTimeLow);

		^chain + reverb

	}
}


// HoaSpatializerReverb {
//
// 	*ar{ arg in, masterX, masterY, masterZ, width, revTimeHigh = 1.0, revTimeLow = 1.0, maxDistance = 100, minDistance = 0.5, mix = 0.25, crossover = 3000, order = AtkHoa.defaultOrder;
// 		var sphere, amplitude, freq, radialDistance;
// 		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
// 		var lpfDB, speedOfSound;
// 		var beam;
// 		var beamAmp;
// 		var reverb;
// 		var listenerHeight, predelay;
//
// 		// lets set a few variables first
// 		hiFreqAttenCoeff = 300000;
// 		minAttenDist = 5;
// 		lpfDB = 3.0;
// 		speedOfSound = 344;
// 		listenerHeight = 1.5;
//
// 		masterX = Lag.kr(masterX);
// 		masterY = Lag.kr(masterY);
// 		masterZ = Lag.kr(masterZ);
//
// 		sphere = Cartesian(masterX, masterY, masterZ);
//
// 		theta = sphere.theta;
// 		phi = sphere.phi;
// 		radialDistance = sphere.rho;
//
// 		predelay = (((radialDistance/2).squared + listenerHeight.squared).sqrt * 2)/speedOfSound - (radialDistance/speedOfSound);
//
// 		// calculate the attenuation of high frequencies based on radial distance
// 		// dB/m = freq/100000
// 		freq = lpfDB/radialDistance * hiFreqAttenCoeff;
//
// 		// the amplitude based on a starting amplitude, amp
// 		amplitude = radialDistance.clip(minDistance, maxDistance).reciprocal;
//
// 		chain = HoaNFCtrl.ar(in, radialDistance.clip(0.5, 10), AtkHoa.refRadius, order);
//
// 		chain = LPF.ar(chain, freq.clip(600, 15000));
//
// 		// use a delay line based on the distance to simulate a doppler shift
// 		chain = DelayN.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);
//
// 		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
// 		chain = HoaZoom.ar(chain, radialDistance.atan2(width), 0, 0, radialDistance.clip(1.0, 10), order);
//
// 		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);
//
// 		reverb = HoaReverb.ar(chain * mix * amplitude.sqrt.sqrt, 1.0, predelay, crossover, t60high: revTimeHigh, t60low: revTimeLow);
//
// 		// lowpass the src and apply envelope and amplitude
// 		// chain = LPF.ar(chain, freq);
//
// 		chain = chain * amplitude.sqrt + reverb;
//
// 		^chain + reverb
//
// 	}
// }