HoaSpatializerP {

	*ar{ arg in, masterX, masterY, masterZ, width, maxDistance = 100, minDistance = 0.5, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;

		// lets set a few variables first
		hiFreqAttenCoeff = 100000;
		minAttenDist = 30;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.linexp(minAttenDist, maxDistance, minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(minDistance, maxDistance).reciprocal;

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(in, freq);

		chain = HoaNFCtrl.ar(chain, radialDistance.clip(0.5, 10), AtkHoa.refRadius, order);

		// use a delay line based on the distance to simulate a doppler shift
		chain = DelayC.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaPush.ar(HPF.ar(chain, 20), radialDistance.atan2(width), 0, 0, radialDistance.clip(0.5, 10), 'energy', 0, order);

		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		^(chain * amplitude)

	}
}

HoaSpatializerNoDelayP {

	*ar{ arg in, masterX, masterY, masterZ, width, maxDistance = 100, minDistance = 0.5, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;

		// lets set a few variables first
		hiFreqAttenCoeff = 100000;
		minAttenDist = 30;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.linexp(minAttenDist, maxDistance, minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(minDistance, maxDistance).reciprocal;

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(in, freq);

		chain = HoaNFCtrl.ar(chain, radialDistance.clip(0.5, 10), AtkHoa.refRadius, order);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaPush.ar(HPF.ar(chain, 20), radialDistance.atan2(width), 0, 0, radialDistance.clip(0.5, 10), 'energy', 0, order);

		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		^(chain * amplitude)

	}
}
HoaEncodeSpatializerP {

	*ar{ arg in, masterX, masterY, masterZ, width, maxDistance = 100, minDistance = 0, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;

		// lets set a few variables first
		hiFreqAttenCoeff = 100000;
		minAttenDist = 30;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.clip(minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(1 + minDistance, maxDistance).reciprocal;

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(in, freq);

		// use a delay line based on the distance to simulate a doppler shift
		// chain = DelayC.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width

		chain = HoaEncodeDirection.ar(HPF.ar(chain, 20), theta, phi, radialDistance.clip(0.5, 10), order);

		^(chain  * amplitude)

	}
}


HoaSpatializerReverbP {

	*ar{ arg in, masterX, masterY, masterZ, width, revTimeHigh = 1.0, revTimeLow = 1.0, maxDistance = 100, minDistance = 0.5, mix = 0.25, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;
		var reverb;
		var listenerHeight, predelay;

		// lets set a few variables first
		hiFreqAttenCoeff = 100000;
		minAttenDist = 30;
		lpfDB = 3.0;
		speedOfSound = 344;
		listenerHeight = 1.5;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		predelay = (((radialDistance/2).squared + listenerHeight.squared).sqrt * 2)/speedOfSound - (radialDistance/speedOfSound);

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.clip(minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(minDistance, maxDistance).reciprocal;

		chain = HoaNFCtrl.ar(in, radialDistance.clip(0.5, 10), AtkHoa.refRadius, order);

		// use a delay line based on the distance to simulate a doppler shift
		chain = DelayC.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaPush.ar(chain, radialDistance.atan2(width), 0, 0, radialDistance.clip(1.0, 10), 'energy', 0, order);

		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		reverb = HoaReverb.ar(chain * mix, 1.0, predelay, t60high: revTimeHigh, t60low: revTimeLow);

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(chain, freq);

		chain = chain * amplitude.sqrt + reverb;

		^chain

	}
}

HoaSpatializerRIR3P {

	*ar{ arg in, masterX, masterY, masterZ, width, partConvBuffers, fftsize, maxDistance = 100, minDistance = 0.5, partDelay = 0.0, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;
		var rir;

		// lets set a few variables first
		hiFreqAttenCoeff = 100000;
		minAttenDist = 30;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.clip(minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(1 + minDistance, maxDistance).reciprocal;

		chain = HoaNFCtrl.ar(in, radialDistance.clip(0.5, 10), AtkHoa.refRadius, order);

		// use a delay line based on the distance to simulate a doppler shift
		chain = DelayC.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);

		rir = HoaDistRIR3.ar(chain[0] * amplitude, partConvBuffers, fftsize, radialDistance.clip(1, 13), theta, phi, order);

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(chain, freq);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaPush.ar(HPF.ar(chain, 40), radialDistance.atan2(width), 0, 0, radialDistance.clip(0.5, 10), 'energy', order);

		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		chain = DelayC.ar(chain, 0.1, partDelay) * amplitude;

		^chain + rir

	}
}

HoaSpatializerRIR3DistP {

	*ar{ arg in, masterX, masterY, masterZ, width, partConvBuffers, fftsize, maxDistance = 100, minDistance = 0.5, partDelay = 0.0, mix = 0.25, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;
		var rir;

		// lets set a few variables first
		hiFreqAttenCoeff = 100000;
		minAttenDist = 30;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.clip(minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(1 + minDistance, maxDistance).reciprocal;

		chain = HoaNFCtrl.ar(in, radialDistance.clip(0.5, 10), AtkHoa.refRadius, order);

		// use a delay line based on the distance to simulate a doppler shift
		chain = DelayC.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);

		rir = HoaDistRIR3.ar(chain[0] * mix, partConvBuffers, fftsize, radialDistance.clip(1, 13), theta, phi, order);

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(chain, freq);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaPush.ar(HPF.ar(chain, 40), radialDistance.atan2(width), 0, 0, radialDistance.clip(minDistance, 10), 'energy', order);

		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		chain = DelayC.ar(chain, 0.1, partDelay) * amplitude;

		^chain + rir

	}
}

HoaSpatializerRIR3DistReverbP {

	*ar{ arg in, masterX, masterY, masterZ, width, partConvBuffers, fftsize, maxDistance = 100, minDistance = 0.5, partDelay = 0.0, mix = 0.25, order = AtkHoa.defaultOrder;
		var sphere, amplitude, freq, radialDistance;
		var phi, theta, chain, hiFreqAttenCoeff, minAttenDist;
		var lpfDB, speedOfSound;
		var beam;
		var beamAmp;
		var rir, reverb;

		// lets set a few variables first
		hiFreqAttenCoeff = 100000;
		minAttenDist = 30;
		lpfDB = 3.0;
		speedOfSound = 344;

		masterX = Lag.kr(masterX);
		masterY = Lag.kr(masterY);
		masterZ = Lag.kr(masterZ);

		sphere = Cartesian(masterX, masterY, masterZ);

		theta = sphere.theta;
		phi = sphere.phi;
		radialDistance = sphere.rho;

		// calculate the attenuation of high frequencies based on radial distance
		// dB/m = freq/100000
		freq = lpfDB/radialDistance.clip(minAttenDist, maxDistance) * hiFreqAttenCoeff;

		// the amplitude based on a starting amplitude, amp
		amplitude = radialDistance.clip(1 + minDistance, maxDistance).reciprocal;

		chain = HoaNFCtrl.ar(in, radialDistance.clip(0.5, 10), AtkHoa.refRadius, order);

		// use a delay line based on the distance to simulate a doppler shift
		// chain = DelayC.ar(chain, maxDistance/speedOfSound, radialDistance/speedOfSound);

		rir = HoaDistRIR3.ar(chain[0] * mix, partConvBuffers, fftsize, radialDistance.clip(1, 13), theta, phi, order);

		// zoom in on the center of the sound-field based on the arctangent of the distance and a user-defined width
		chain = HoaPush.ar(HPF.ar(chain, 40), radialDistance.atan2(width), 0, 0, radialDistance.clip(0.5, 10), 'energy', order);

		chain = HoaRotate.ar(HoaTumble.ar(chain, phi, order), theta, order);

		reverb = HoaReverb.ar(chain * mix * 2);

		// lowpass the src and apply envelope and amplitude
		chain = LPF.ar(chain, freq);

		chain = DelayC.ar(chain, 0.1, partDelay) * amplitude;

		^chain + rir + reverb

	}
}